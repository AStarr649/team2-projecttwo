## Name
Project Two - Movie Ticket Applicaton

## Description
Users can select a currently playing movie, and a corresponding show time, to purchase ticket(s) for. Users can optionally register for an account to earn reward points for purchasing tickets, and are able to recover their password if forgotten. This is a joint project using Spring and Angular collaborated between several members.

## Authors and acknowledgment
Alex Starr      - Project Lead
Prabin Pariya   - Spring Developer
Giselle Cobo    - Angular Developer
Erandy Burciaga - Lead Design Developer

## License
SpringBoot 2.6.3
Angular 13.1.4
Eclipse IDE 2021-12 (4.22.0)
VisualStudio Code 1.64.1

## Project status
In Progress(2/14/2022)