import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Movie } from '../interface/movie';
import { DatabaseService } from '../service/database.service';

@Component({
  selector: 'app-movie-info',
  templateUrl: './movie-info.component.html',
  styleUrls: ['./movie-info.component.css']
})
export class MovieInfoComponent implements OnInit {

  movie: Movie;
  showAllTimes = false;



  constructor(
    private route: ActivatedRoute,
    private db: DatabaseService,
        ) {}



    ngOnInit()  {
    this.getMovie();
    }
    getMovie(): void {
    const id = +this.route.snapshot.paramMap.get('id');

    //this.db.getMovie(id).subscribe(movie => this.movie = movie);
  }

  // show all showtimes
  showAllShowtimes(): void {
    this.showAllTimes = true;
  }

  // hide all showtimes
  hideAllShowtimes(): void {
    this.showAllTimes = false;
  }

  
  getEmbedUrl(url: string) {
    return url.replace('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/');
  }
}





