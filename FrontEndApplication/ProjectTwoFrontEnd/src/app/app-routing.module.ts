import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserHomeComponent } from './user-home/user-home.component';
import { MovieInfoComponent } from './movie-info/movie-info.component';
import { TicketnSeatComponent } from './ticketn-seat/ticketn-seat.component';
import { UserRegistrationComponent } from './user-registration/user-registration.component';
import { ProfileComponent } from './profile/profile.component';
import { UserReservationComponent } from './user-reservation/user-reservation.component';

const routes: Routes = [
  {path: 'login', component: UserLoginComponent},
  {path: 'registration', component: UserRegistrationComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'userhome', component: UserHomeComponent},
  {path: 'movieinfo', component: MovieInfoComponent},
  {path: 'selection', component: TicketnSeatComponent},
  {path: 'ticketn-seat/:movieId', component: TicketnSeatComponent},
  {path: 'reservation', component: UserReservationComponent},
  {path: '**', redirectTo:'login'}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
