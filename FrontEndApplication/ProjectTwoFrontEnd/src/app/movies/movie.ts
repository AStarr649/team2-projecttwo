import { Time } from "@angular/common";

export class Movie{
    constructor(public title:string, public description:string, public duration:Time, public language:string, public releasedDate:Date, public country:string, public genre:string, public movieId?:number){}
}