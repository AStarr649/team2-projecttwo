import { AfterViewChecked, AfterViewInit, Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { ColdObservable } from 'rxjs/internal/testing/ColdObservable';
import { User } from './user-login/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements  OnInit, OnChanges{
  
  isNavShowing: boolean = false; 
  title = 'ProjectTwoFrontEnd';

  constructor(private router: Router){}
  
  ngOnChanges(changes: SimpleChanges): void {
    let user = JSON.parse(localStorage.getItem("sessionUser")) as User;
    if(!user){
      this.isNavShowing=false;
    }else if(user){
      this.isNavShowing=true;
    }
    console.log(this.isNavShowing);
    console.log(user);
  }

  ngAfterViewInit(): void {
    console.log("I am inside ngAfterViewInit");
  }
  ngOnInit(): void {
    console.log("I am inside ngAfterViewChecked");
    
  }

  changeIsNavShowing(condition: boolean){
    console.log(condition);
    this.isNavShowing = condition;
  }
  
  
}
declare var test: any;


