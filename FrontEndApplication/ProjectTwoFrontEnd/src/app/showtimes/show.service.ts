import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Show } from './show';

@Injectable({
  providedIn: 'root'
})
export class ShowService {

  private urlBase = 'http://localhost:9065/shows';

  httpHead = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };
  constructor(private http: HttpClient) { }
  public getShowByMovieId(movieId:string): Observable<Show[]>{
    return this.http.get<Show[]>(this.urlBase+"/movie/"+movieId, this.httpHead);
  }

}
