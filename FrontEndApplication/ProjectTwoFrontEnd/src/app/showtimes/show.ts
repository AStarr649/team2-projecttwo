import { Time } from "@angular/common";
import { Movie } from "../movies/movie";
import { Hall } from "./hall";

export class Show{
    constructor(public showDate: Date, public startTime: Time, public hHolder: Hall, public mHolder: Movie, public showId?:number){}
}