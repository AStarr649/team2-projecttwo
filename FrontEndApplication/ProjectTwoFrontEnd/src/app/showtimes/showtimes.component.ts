import { Component, OnInit  } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Show } from './show';
import { ShowService } from './show.service';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-showtimes', 
  templateUrl: './showtimes.component.html',
  styleUrls: ['./showtimes.component.css']
})

export class ShowtimesComponent implements OnInit  {
 

  movieId:string;
  showList: Show[] = [];

  @Output() newItemEvent = new EventEmitter<Show>();

  constructor(private showServ: ShowService, private router: Router, private actRoute:ActivatedRoute) { }

  ngOnInit(): void {
    this.movieId = this.actRoute.snapshot.paramMap.get('movieId');

    this.showServ.getShowByMovieId(this.movieId).subscribe(
      response => {
        this.showList=response;
        this.addNewItem(this.showList[0]);

      }
    )
    

  }

  addNewItem(value: Show){
    console.log(value);
    this.newItemEvent.emit(value);
  }

}


