import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserHomeComponent } from './user-home/user-home.component';
import { CommonModule } from '@angular/common';
import { TicketnSeatComponent } from './ticketn-seat/ticketn-seat.component';
import { SeatComponent } from './seat/seat.component';
import { UserRegistrationComponent } from './user-registration/user-registration.component';
import { ProfileComponent } from './profile/profile.component';
import { MovieInfoComponent } from './movie-info/movie-info.component';
import { ShowtimesComponent } from './showtimes/showtimes.component';
import { UserReservationComponent } from './user-reservation/user-reservation.component';
import { NavComponent } from './nav/nav.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    UserLoginComponent,ShowtimesComponent,
    UserHomeComponent, TicketnSeatComponent, SeatComponent,
    UserRegistrationComponent, MovieInfoComponent, UserReservationComponent, NavComponent
    ,  ProfileComponent
    
  ],
  imports: [
    BrowserModule, CommonModule, 
    AppRoutingModule, FormsModule, ReactiveFormsModule, HttpClientModule, NgbModule
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
