import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Event, NavigationStart, Router } from '@angular/router';
import { User } from '../user-login/user';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  showNav = false;
  showProfile = true;
  constructor(private router: Router, private ref: ChangeDetectorRef) {
    router.events.subscribe(
      (event: Event)=>{
        if(event instanceof NavigationStart){
          this.showNav = true;
          this.showProfile = true;
          console.log(event.url);
          if(event.url == "/login" || event.url =="/registration"){
            this.showNav = false;
          }
          let user = JSON.parse(localStorage.getItem("sessionUser")) as User;
          if(user){
            if(user.username == "guest046"){
              this.showProfile = false;
            }
          }
          console.log(this.showNav);
          this.ref.detectChanges();
        }
      }
    )
   }

  ngOnInit(): void {
  }

}
