import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from './user';
import { UserService } from './user.service';
import { UserPwd } from './userPwd';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {

  userForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl('')
  });

  userNotFound: boolean = false;
  errorMessage: string; 
  username: string;
  usernamePvalue = "UserName";
  usernametype = "text";
  eyeForUsername = false;
  
  eyeForPassword = false;
  passwordtype = "password";
  passwordPvalue = "Password";
  loginBtnValue = "LOGIN";
  boxtitleValue = "Sign In";

  constructor(private userServ: UserService, public router: Router) { }

  ngOnInit(): void {
    localStorage.removeItem('sessionUser');
  }

  public login(userForm:FormGroup){
    
    let stringUser = JSON.stringify(userForm.value);
    this.userServ.login(stringUser).subscribe(
      response => {
        
        this.userNotFound=false;
        console.log("response: "+response);
        localStorage.setItem("sessionUser", JSON.stringify(response));
        this.router.navigate(['/userhome']);
      },
      error => {
        this.errorMessage = "Username or Password Mismatch...!";
        this.userNotFound=true;
      }
    )
 }

  public forgetPassword(userForm:FormGroup){
    this.userNotFound = false;
    let userName = userForm.get("username").value;
    console.log(userName);
    if(this.loginBtnValue == "LOGIN"){
      if (userName == ""){
        this.errorMessage = "Username Should not be Empty...!";
        this.userNotFound=true;
        console.log("Empty");
      }else{
        this.userServ.forgotPassword(userName).subscribe(
          response => {
            this.userNotFound=false;
            console.log("response: "+response);
            console.log("Inside Response");
            this.username = userName;
            userForm.reset();
            this.usernamePvalue = "Old Password";
            this.passwordPvalue = "New Password";
            this.loginBtnValue = "Reset Password";
            this.boxtitleValue = "Forget Password";
            this.usernametype = "password";
            this.eyeForUsername = true;
            this.eyeForPassword = true;
           },
          error => {
            this.errorMessage = "Username Not Found...!"
            this.userNotFound=true;
            console.log("Inside Error");
          }
        )
      }
    }
    
   

  }

  public decideFunction(userForm:FormGroup){
    if(this.loginBtnValue=="LOGIN"){
      this.login(userForm);
    }else if(this.loginBtnValue == "Reset Password"){
      this.resetPassword(userForm);
    }
  }

  public resetPassword(userForm:FormGroup){
    
    let userpwd: UserPwd = new UserPwd(this.username, userForm.get("username").value, userForm.get("password").value);
    console.log("Inside Reset Password");
    let userpwdString = JSON.stringify(userpwd);
    console.log(userpwdString);
    this.userServ.resetPassword(userpwdString).subscribe(
      response =>{
        console.log(response);
        userForm.reset();
        this.usernamePvalue = "UserName";
        this.passwordPvalue = "Password";
        this.loginBtnValue = "LOGIN";
        this.boxtitleValue = "Sign In";
        this.userNotFound=false;
        this.eyeForUsername = false;
        this.eyeForPassword = false;
        this.usernametype = "text";
        this.passwordtype = "password";
      },
      error => {
        this.errorMessage = "Old Password is Incorrect...!"
        this.userNotFound=true;
      }
    )
  }

  public toggleUsername(){
    console.log("Clicked Username toggler");
    if(this.usernametype=="text"){
      this.usernametype="password";
    }else if(this.usernametype =="password"){
      this.usernametype="text";
    }
  }
  public togglePassword(){
    console.log("Clicked Password toggler");
    if(this.passwordtype=="text"){
      this.passwordtype="password";
    }else if(this.passwordtype=="password"){
      this.passwordtype="text";
    }
  }
  public guestLogin(userForm:FormGroup){
    userForm.setValue({
      username: 'guest046',
      password: 'guest046'
    });
    this.login(userForm);
  }
}



