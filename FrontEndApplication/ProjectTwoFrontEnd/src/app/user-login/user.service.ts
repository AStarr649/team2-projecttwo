import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './user';
import { UserPwd } from './userPwd';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private urlBase = "http://localhost:9065/users";

  httpHead = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };

 

  constructor(private http: HttpClient) { }

  public registerUser(user: string): Observable<User>{
    return this.http.post<User>(this.urlBase, user, this.httpHead);
  }

  public login(user: string): Observable<User>{
    return this.http.post<User>(this.urlBase+"/login", user, this.httpHead);
  }

  public forgotPassword(userName: string): Observable<any>{
    //return this.http.post<string>(this.urlBase+"/user/pwreset/"+userName, this.httpOptions);
    const httpOptions: Object = {
      headers: new HttpHeaders({
        'Accept': 'text/html, application/xhtml+xml, */*',
        'Access-Control-Allow-Origin':'*',
        'Content-Type': 'application/x-www-form-urlencoded'
      }),
      responseType: 'text'
    };
    return this.http.post(this.urlBase+"/user/pwreset/"+userName, "Test", httpOptions);
  }

  public resetPassword(userPwd: string): Observable<User>{
    return this.http.post<User>(this.urlBase+"/user/newpassword", userPwd, this.httpHead);
  }
}
