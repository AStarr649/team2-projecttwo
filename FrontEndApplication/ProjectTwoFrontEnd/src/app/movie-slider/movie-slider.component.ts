import { Component, Input, OnInit } from '@angular/core';
import { Movie } from '../interface/movie';
import { DatabaseService } from '../service/database.service';

@Component({
  selector: 'app-movie-slider',
  templateUrl: './movie-slider.component.html',
  styleUrls: ['./movie-slider.component.css']
})
export class MovieSliderComponent implements OnInit {
  @Input() limit: number;
  movies: Movie[];


  constructor(private db: DatabaseService) { }

  ngOnInit()  {
    this.getMovies();
  }
  getMovies() {
    this.db.getMovies(this.limit).subscribe(movies => this.movies = movies);;
}

}


