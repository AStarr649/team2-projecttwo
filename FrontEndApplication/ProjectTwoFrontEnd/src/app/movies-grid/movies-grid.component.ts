import { Component, Input, OnInit } from '@angular/core';
import { Movie } from '../interface/movie';
import { DatabaseService } from '../service/database.service';

@Component({
  selector: 'app-movies-grid',
  templateUrl: './movies-grid.component.html',
  styleUrls: ['./movies-grid.component.css']
})
export class MoviesGridComponent implements OnInit {
  @Input() limit: number;
  @Input() columns: number;
  @Input() exclude?: number | number[];
  movies: Movie[];
  previewUrl = '';





  constructor(private db: DatabaseService,) { }

  ngOnInit() {
    this.getMovies();
  }
  getMovies() {
    this.db.getMovies(this.limit, this.exclude).subscribe(movies => this.movies = movies);;
  }
  
  
  getEmbedUrl(url: string) {
    return url.replace('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/');
}
}