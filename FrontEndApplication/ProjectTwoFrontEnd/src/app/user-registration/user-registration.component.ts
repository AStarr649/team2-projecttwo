import { FormatWidth } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../user-login/user.service';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {
  
  registrationForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    email: new FormControl('')
  });

  constructor(private userServ: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  public submitUser(user: FormGroup){
    let stringUser = JSON.stringify(user.value);
    this.userServ.registerUser(stringUser).subscribe(
      response => {
        console.log(response);
        this.router.navigate(['/userhome']);
      }
    )
  }

  public backToLogin(){
    this.router.navigate(['/login']);
  }

}
