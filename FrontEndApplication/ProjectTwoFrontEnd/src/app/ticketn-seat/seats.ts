export class Seat{
    constructor(public seatNumber: number, public price: number, public booked: boolean, public show: object){}
}