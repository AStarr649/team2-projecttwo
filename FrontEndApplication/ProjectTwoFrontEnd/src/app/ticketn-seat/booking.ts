import { Show } from "../showtimes/show";
import { User } from "../user-login/user";
import { Seat } from "./seats";

export class Booking {
    constructor(public sHolder: Show, public uHolder: User, public seats: Seat[], public bookingId?: number,public bookingDate?: Date){}
}