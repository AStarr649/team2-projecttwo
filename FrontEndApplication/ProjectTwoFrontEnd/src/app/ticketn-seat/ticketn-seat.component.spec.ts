import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketnSeatComponent } from './ticketn-seat.component';

describe('TicketnSeatComponent', () => {
  let component: TicketnSeatComponent;
  let fixture: ComponentFixture<TicketnSeatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TicketnSeatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketnSeatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
