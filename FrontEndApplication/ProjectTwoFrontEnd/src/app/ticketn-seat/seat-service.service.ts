import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Show } from '../showtimes/show';
import { Booking } from './booking';
import { Seat } from './seats';

@Injectable({
  providedIn: 'root'
})
export class SeatServiceService {

  private urlBase = "http://localhost:9065";
  httpHead = {
    headers: new HttpHeaders({
      'Content-Type':'application/json',
      'Access-Control-Allow-Origin':'*'
    })
  }

  constructor(private http: HttpClient) { }

  public getAllSeats(): Observable<Seat[]>{
    return this.http.get<Seat[]>(this.urlBase, this.httpHead);
  }

  public getAllSeatsByShow(show: Show): Observable<Seat[]>{
    return this.http.post<Seat[]>(this.urlBase+"/seats/show", show, this.httpHead);
  }

  public insertBooking(booking: Booking): Observable<Booking>{
    return this.http.post<Booking>(this.urlBase+"/bookings", booking, this.httpHead);
  }
}
