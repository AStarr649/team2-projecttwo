import { DatePipe, formatDate, getLocaleDateFormat } from '@angular/common';
import { Component, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Movie } from '../movies/movie';
import { Show } from '../showtimes/show';
import { User } from '../user-login/user';
import { Booking } from './booking';
import { SeatServiceService } from './seat-service.service';
import { Seat } from './seats';

declare var test: any;



@Component({
  selector: 'app-ticketn-seat',
  templateUrl: './ticketn-seat.component.html',
  styleUrls: ['./ticketn-seat.component.css'],
  providers: [DatePipe]
})
export class TicketnSeatComponent implements OnInit, OnChanges {

  selectedCount = 0;
  seatList: Seat[] = [];
  booking: Booking;

  selectedSeatList: Seat[] = [];

  aSeat:Seat;
  show: Show;
  user: User;
  imageName: string;
  movie: Movie;
  
  

  constructor(private seatServ: SeatServiceService, private router: Router) { }
  ngOnChanges(changes: SimpleChanges): void {
    
  }

  ngOnInit(): void {
    let dt = new Date();
    console.log(dt);
    
  } 
  addItem(show: Show){

   this.imageName= "/assets/img_movie/";
    this.show = show;
    console.log(this.show.mHolder);
    let movieId = show.mHolder.movieId;
    let movieTitle = show.mHolder.title;
    let movieDesc = show.mHolder.description;
    if(movieId == 1){
      this.imageName += "spider.jpg";
      document.getElementById("featureTitle").innerText = movieTitle;
      document.getElementById("featureDesc").innerText = movieDesc;
    }else if(movieId == 2){
      this.imageName += "sing.jpg";
      document.getElementById("featureTitle").innerText = movieTitle;
      document.getElementById("featureDesc").innerText = movieDesc;
    }else if(movieId == 3){
      this.imageName += "moonfall.jpg";
      document.getElementById("featureTitle").innerText = movieTitle;
      document.getElementById("featureDesc").innerText = movieDesc;
    }else if(movieId == 4){
        this.imageName += "uncharted.jpg";
        document.getElementById("featureTitle").innerText = movieTitle;
        document.getElementById("featureDesc").innerText = movieDesc;
    }
    console.log(this.imageName);

    this.seatServ.getAllSeatsByShow(this.show).subscribe(
      response =>{
        console.log(response);
        this.seatList=response;
        console.log(this.seatList);
      }
    )
  }
  
  seatCount(seat: Seat){
    console.log(seat.booked);
    if(!seat.booked){
      if(!this.selectedSeatList.includes(seat)){
        this.selectedCount++;
        console.log(seat);
        this.selectedSeatList.push(seat);
      }else{
        this.selectedCount--;
        let index = this.selectedSeatList.indexOf(seat);
        this.selectedSeatList.splice(index, 1);
      }
    }
    
    console.log(this.selectedSeatList);
  }
  submitBooking(){
    console.log("submitBooking clicked");
    this.user = JSON.parse(localStorage.getItem("sessionUser")) as User;
    if(this.user.username == "guest046"){
      console.log("For Booking you need to login first");
    }else{
      this.booking = new Booking(this.show, this.user, this.selectedSeatList);
      console.log(this.booking);
      this.seatServ.insertBooking(this.booking).subscribe(
      response =>{
        console.log(response);
        console.log("On Ticketn: " + this.show.mHolder.title + " "+this.show.hHolder.name );
        this.router.navigate(['/reservation'],{queryParams:{title: this.show.mHolder.title, movietime: this.show.startTime, seats: this.getSeatsAsString(this.booking)}});
      }
    )
    }
  }

  private getSeatsAsString(booking: Booking): string {
    let seatTemp = "";
    for(let seat of booking.seats){
      seatTemp += seat.seatNumber+ ", ";
    }
    return seatTemp;
  }
}
