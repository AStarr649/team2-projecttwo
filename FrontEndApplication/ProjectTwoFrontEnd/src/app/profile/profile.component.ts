import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user-login/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user: User;
  constructor(public router: Router) { }

  ngOnInit(): void {
    let myuser = JSON.parse(localStorage.getItem("sessionUser")) as User;
    if(!myuser){
      this.router.navigate(["/login"]);
    }
    this.user = myuser;
  
  }

}
