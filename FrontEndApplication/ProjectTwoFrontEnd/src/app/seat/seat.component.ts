import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Seat } from '../ticketn-seat/seats';


@Component({
  selector: 'app-seat',
  templateUrl: './seat.component.html',
  styleUrls: ['./seat.component.css']
})
export class SeatComponent implements OnInit {

  clicked = false;
  @Input()
  booked = false;

  @Input()
  aSeat: Seat;
  // seat variable that is when dynamically made, that the seat object is sent in, change the property of booked in isSelected() and send that instead

  @Output()
  seatClicked = new EventEmitter<Seat>()

  constructor() { }

  ngOnInit(): void {
    
  }

  public isSelected(){
  console.log("something");
    this.clicked = !this.clicked;
    console.log(this.aSeat);
    this.seatClicked.emit(this.aSeat);
  }

}
