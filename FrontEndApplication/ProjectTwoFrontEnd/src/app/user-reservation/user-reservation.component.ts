import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Booking } from '../ticketn-seat/booking';

@Component({
  selector: 'app-user-reservation',
  templateUrl: './user-reservation.component.html',
  styleUrls: ['./user-reservation.component.css']
})
export class UserReservationComponent implements OnInit {

  booking: Booking;
  movieTitle: string;
  movieTime: string;
  seats: string;

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    console.log(this.route.snapshot.queryParamMap.get('title'));
    console.log(this.route.snapshot.queryParamMap.get('movietime'));
    console.log(this.route.snapshot.queryParamMap.get('seats'));

    this.movieTitle = this.route.snapshot.queryParamMap.get('title');
    this.movieTime = this.route.snapshot.queryParamMap.get('movietime');
    this.seats = this.route.snapshot.queryParamMap.get('seats');
  }

  gotoHome(){
    this.router.navigate(['/userhome']);
  }

}
