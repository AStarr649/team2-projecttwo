import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user-login/user';
import { NgForm } from '@angular/forms';
import { Movie } from '../movies/movie';
import { MovieService } from './movie.service';

@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.css']
})
export class UserHomeComponent implements OnInit {

  movieList: Movie[] = [];

  constructor(private movieServ: MovieService, private router: Router) { }

  ngOnInit(): void {
    let user = JSON.parse(localStorage.getItem("sessionUser")) as User;
    if(!user){
      this.router.navigate(["/login"]);
    }
    this.movieServ.getFeaturedMovies().subscribe(
      response =>{
        this.movieList=response;
      }
    )
  }

}

