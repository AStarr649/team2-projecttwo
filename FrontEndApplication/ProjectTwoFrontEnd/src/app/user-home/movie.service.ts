import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Movie } from '../movies/movie';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  private urlBase="http://localhost:9065/movies/featured";

  httpHead = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };
  
  constructor(private http: HttpClient) { }

  public getFeaturedMovies(): Observable<Movie[]>{
    return this.http.get<Movie[]>(this.urlBase, this.httpHead);
  }
}
