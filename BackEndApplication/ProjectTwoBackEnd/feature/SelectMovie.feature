Feature: A User can select a movie and show time to purchase ticket(s)

Background: A user is logged in on the home page
		Given The user is on the login page
		When the user inputs "dummyTest" into the username field
		When the user inputs "aPass" into the password field
		When the user clicks login
		Then the user is directed to the home page

  Scenario: A user can view all current movies playing
    When movie home page loads
    When user clicks movie
    Then user should see show times available
