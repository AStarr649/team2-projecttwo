Feature: A user can log into the home page

Scenario: Logging into the main page
		Given The user is on the login page
		When the user inputs "dummyTest" into the username field
		When the user inputs "aPass" into the password field
		When the user clicks login
		Then the user is directed to the home page