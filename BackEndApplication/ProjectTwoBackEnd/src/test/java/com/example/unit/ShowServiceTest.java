package com.example.unit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.model.Hall;
import com.example.model.Movie;
import com.example.model.Show;
import com.example.repository.ShowRepo;
import com.example.service.ShowService;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class ShowServiceTest {

	@InjectMocks
	ShowService service;
	
	@MockBean
	ShowRepo mockRepo;
	
	Show show;
	List<Show> sList = new ArrayList<Show>();
	LocalDate date;
	LocalTime time;
	Hall hall;
	Movie movie;
	
	@BeforeEach
	public void setUp() {
		hall = new Hall(9, "Silver", 45);
		movie = new Movie(9, "Vox Machina", "animated insanity", time.NOON, "English", date.now(), "US", "Animation");
		show = new Show(87, date.now(), time.NOON, hall, movie);
		sList.add(show);
	}
	
	@Test
	public void testInsertShow() {
		when(this.mockRepo.save(show)).thenReturn(show);
		this.service.insertShow(show);
		verify(this.mockRepo, times(1)).save(show);
	}
	
	@Test
	public void testFindAll() {
		when(this.service.findAllShow()).thenReturn(sList);
		assertEquals(sList, service.findAllShow());
	}
	
	@Test
	public void testFindByShowDate() {
		when(this.service.findShowByShowdate(date)).thenReturn(sList);
		assertEquals(sList, service.findShowByShowdate(date));
	}
	
	@Test
	public void testFindByShowTime() {
		when(this.service.findShowByStarttime(time)).thenReturn(sList);
		assertEquals(sList, service.findShowByStarttime(time));
	}
	
	@Test
	public void testFindByMovieId() {
		when(this.mockRepo.findAll()).thenReturn(sList);
		when(this.service.findShowByMovieId(movie.getMovieId())).thenReturn(sList);
		assertEquals(sList, service.findShowByMovieId(9));
	}
	
	@Test
	public void testFindByHallId() {
		when(this.mockRepo.findAll()).thenReturn(sList);
		when(this.service.findShowByHallId(hall.getHallId())).thenReturn(sList);
		assertEquals(sList, service.findShowByHallId(9));
	}
}
