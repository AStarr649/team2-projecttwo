package com.example.unit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.model.Movie;
import com.example.repository.MovieRepo;
import com.example.service.MovieService;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class MovieServiceTest {

	@InjectMocks
	MovieService service;
	@MockBean
	MovieRepo mockRepo;
	
	Movie movie;
	LocalDate date;
	LocalTime time;
	List<Movie> mList = new ArrayList<Movie>();
	
	@BeforeEach
	public void setUp() {
		movie = new Movie(9, "Vox Machina", "animated insanity", time.NOON, "English", date.now(), "US", "Animation");
	}
	
	@Test
	public void testInsertMovie() {
		when(this.mockRepo.save(movie)).thenReturn(movie);
		this.service.insertMovie(movie);
		verify(this.mockRepo, times(1)).save(movie);
	}
	
	@Test
	public void testFindAll() {
		when(this.service.findAll()).thenReturn(mList);
		assertEquals(mList, service.findAll());
	}
	
	@Test
	public void testFindByTitle() {
		when(this.service.findByTitle("Vox Machina")).thenReturn(movie);
		assertEquals(movie, service.findByTitle("Vox Machina"));
	}
	
	@Test
	public void testFindByLanguage() {
		when(this.service.findByLanguage("English")).thenReturn(mList);
		assertEquals(mList, service.findByLanguage("English"));
	}
	
	@Test
	public void testFindByCountry() {
		when(this.service.findByCountry("US")).thenReturn(mList);
		assertEquals(mList, service.findByCountry("US"));
	}
	
	@Test
	public void testFindByGenre() {
		when(this.service.findByGenre("Animation")).thenReturn(mList);
		assertEquals(mList, service.findByGenre("Animation"));
	}
}
