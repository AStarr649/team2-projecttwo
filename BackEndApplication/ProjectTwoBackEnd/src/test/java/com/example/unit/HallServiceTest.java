package com.example.unit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.model.Hall;
import com.example.repository.HallRepo;
import com.example.service.HallService;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class HallServiceTest {

	@InjectMocks
	HallService service;
	@MockBean
	HallRepo mockRepo;
	
	Hall hall;
	List<Hall> hList = new ArrayList<Hall>();
	
	@BeforeEach
	public void setUp() {
		hall = new Hall(9, "Silver", 45);
	}
	
	@Test
	public void testInsertHall() {
		when(this.mockRepo.save(hall)).thenReturn(hall);
		this.service.insertHall(hall);
		verify(this.mockRepo, times(1)).save(hall);
	}
	
	@Test
	public void testFindAll() {
		when(this.service.findAll()).thenReturn(hList);
		assertEquals(hList, service.findAll());
	}
	
	@Test
	public void testFindByName() {
		when(this.service.findByName("Silver")).thenReturn(hall);
		assertEquals(hall, service.findByName("Silver"));
	}
}
