package com.example.unit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.model.Booking;
import com.example.model.Hall;
import com.example.model.Movie;
import com.example.model.Seat;
import com.example.model.Show;
import com.example.model.User;
import com.example.repository.BookingRepo;
import com.example.service.BookingService;
import com.example.service.SeatService;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class BookingServiceTest {

	@InjectMocks
	BookingService service;
	@MockBean
	SeatService seatService;
	@MockBean
	BookingRepo mockRepo;
	
	Booking book;
	LocalDate date;
	LocalTime time;
	Show show;
	User user;
	Hall hall;
	Movie movie;
	Seat seat;
	List<Booking> bList = new ArrayList<Booking>();
	List<Seat> sList = new ArrayList<Seat>();
	
	@BeforeEach
	public void setUp() {
		user = new User(54, "aUser", "aPassword", "Dave", "Smith", "dave@email.com");
		hall = new Hall(9, "Silver", 45);
		movie = new Movie(9, "Vox Machina", "animated insanity", time.NOON, "English", date.now(), "US", "Animation");
		show = new Show(87, date.now(), time.NOON, hall, movie);
		seat = new Seat(7, 1, 12.00, false, show);
		sList.add(seat);
		book = new Booking(4, date.now(), show, user, sList);
	}
	
	@Test
	public void testInsertBooking() {		
		when(this.mockRepo.save(book)).thenReturn(book);
		doNothing().when(this.seatService).insertSeat(seat);
		this.service.insertBooking(book);
		verify(this.mockRepo, times(1)).save(book);
	}
	
	@Test
	public void testFindAll() {
		when(this.service.findAll()).thenReturn(bList);
		assertEquals(bList, service.findAll());
	}
	
	@Test
	public void testFindBookingByBookID() {
		when(this.service.findBookingByBookingId(4)).thenReturn(book);
		assertEquals(book, service.findBookingByBookingId(4));
	}
}
