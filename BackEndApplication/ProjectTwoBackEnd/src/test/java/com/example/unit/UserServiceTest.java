package com.example.unit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.model.User;
import com.example.repository.UserRepo;
import com.example.service.UserService;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class UserServiceTest {

	@InjectMocks
	UserService service;	
	@MockBean
	UserRepo mockRepo;
	
	User user;
	List<User> uList = new ArrayList<User>();
	
	@BeforeEach
	public void setUp() {
		user = new User(54, "aUser", "aPassword", "Dave", "Smith", "dave@email.com");
		uList.add(user);
	}
	
	@Test
	public void testGetAllUsers(){
		when(this.service.getAllUsers()).thenReturn(uList);
		assertEquals(uList, service.getAllUsers());
	}
	
	@Test
	public void testGetUserByName() {
		when(this.service.getUserByUsername("aUser")).thenReturn(user);
		assertEquals(user, service.getUserByUsername(user.getUsername()));
	}
	
	@Test
	public void testFindUserByEmail() {
		when(this.service.findUserByEmail("dave@email.com")).thenReturn(user);
		assertEquals(user, service.findUserByEmail(user.getEmail()));
	}
	
	@Test
	public void testVerifyUserLogin() {
		when(this.service.verifyUserLogin("aUser", "aPassword")).thenReturn(user);
		assertEquals(user, service.verifyUserLogin("aUser", "aPassword"));
	}
	
	@Test
	public void testInsertNewUser() {
		when(this.mockRepo.save(user)).thenReturn(user);
		this.service.insertNewUser(user);
		verify(this.mockRepo, times(1)).save(user);
	}
	
	@Test
	public void testChangeUserPassword() {
		when(this.mockRepo.save(user)).thenReturn(user);
		when(this.mockRepo.getById(user.getUserId())).thenReturn(user);
		this.service.changeUserPassword("newPas", 54);
		verify(this.mockRepo, times(1)).save(user);
	}
}
