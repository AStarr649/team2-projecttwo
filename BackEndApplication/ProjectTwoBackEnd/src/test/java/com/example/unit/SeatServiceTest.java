package com.example.unit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.model.Hall;
import com.example.model.Movie;
import com.example.model.Seat;
import com.example.model.Show;
import com.example.repository.SeatRepo;
import com.example.service.SeatService;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class SeatServiceTest {

	@InjectMocks
	SeatService service;
	@MockBean
	SeatRepo mockRepo;
	
	Seat seat;
	Show show;
	LocalDate date;
	LocalTime time;
	Hall hall;
	Movie movie;
	List<Seat> sList = new ArrayList<Seat>();
	
	@BeforeEach
	public void setUp() {
		hall = new Hall(9, "Silver", 1);
		movie = new Movie(9, "Vox Machina", "animated insanity", time.NOON, "English", date.now(), "US", "Animation");
		show = new Show(87, date.now(), time.NOON, hall, movie);
		seat = new Seat(7, 1, 12.00, false, show);
		sList.add(seat);
	}
	
	@Test
	public void testInsertSeat() {
		when(this.mockRepo.save(seat)).thenReturn(seat);
		this.service.insertSeat(seat);
		verify(this.mockRepo, times(1)).save(seat);
	}
	
	@Test
	public void testFindAll() {
		when(this.service.findAll()).thenReturn(sList);
		assertEquals(sList, service.findAll());
	}
	
	@Test
	public void testFindBySeatId() {
		when(this.service.findBySeatId(7)).thenReturn(seat);
		assertEquals(seat, service.findBySeatId(7));
	}
	
	@Test
	public void testFindBySeatNumber() {
		when(this.service.findBySeatNumber(1)).thenReturn(seat);
		assertEquals(seat, service.findBySeatNumber(1));
	}
	
	@Test
	public void testFindByIsBooked() {
		when(this.service.findByIsBooked(false)).thenReturn(sList);
		assertEquals(sList, service.findByIsBooked(false));
	}
	
	@Test
	public void testFindByShow() {
		when(this.mockRepo.findAll()).thenReturn(sList);
		when(this.service.findByShow(show)).thenReturn(sList);
		assertEquals(sList, service.findByShow(show));
	}
}
