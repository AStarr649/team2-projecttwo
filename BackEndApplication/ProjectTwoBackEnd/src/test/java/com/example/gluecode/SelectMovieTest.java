package com.example.gluecode;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.page.HomePage;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class SelectMovieTest {

	public HomePage apeg;
	
	@When("movie home page loads")
	public void movie_home_page_loads() {
		this.apeg = new HomePage(APEGDriverUtility.driver);
	}

	@When("user clicks movie")
	public void user_clicks_movie() {
	    apeg.clickMovieButton();
	}

	@Then("user should see show times available")
	public void user_should_see_show_times_available() {
		WebDriverWait wait = new WebDriverWait(APEGDriverUtility.driver, 5);
	    wait.until(ExpectedConditions.urlContains("seat"));
	    assertEquals("13:00", APEGDriverUtility.driver.findElement(By.tagName("button")).getText());
	}

}
