package com.example.gluecode;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.page.LoginPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class APEGLoginTest {

	public LoginPage apeg;
	public String userName, password;
	
	@Given("The user is on the login page")
	public void the_user_is_on_the_login_page() {
	    this.apeg = new LoginPage(APEGDriverUtility.driver);
	    assertEquals("Apeg Movie Theater", APEGDriverUtility.driver.getTitle());
	}

	@When("the user inputs {string} into the username field")
	public void the_user_inputs_into_the_username_field(String string) {
		this.userName = string;
	}

	@When("the user inputs {string} into the password field")
	public void the_user_inputs_into_the_password_field(String string) {
	    this.password = string;
	}

	@When("the user clicks login")
	public void the_user_clicks_login() {
	    apeg.loginToHomePage(userName, password);
	}

	@Then("the user is directed to the home page")
	public void the_user_is_directed_to_the_home_page() {
	    WebDriverWait wait = new WebDriverWait(APEGDriverUtility.driver, 5);
	    wait.until(ExpectedConditions.urlContains("userhome"));
	    assertEquals("PROFILE", APEGDriverUtility.driver.findElement(By.tagName("a")).getText());
	}

}
