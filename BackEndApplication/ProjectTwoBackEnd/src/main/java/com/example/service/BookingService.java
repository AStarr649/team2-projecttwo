package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Booking;
import com.example.model.Seat;
import com.example.repository.BookingRepo;

@Service
public class BookingService {
	
	private BookingRepo bRepo;
	private SeatService sServ;
	
	public BookingService() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public BookingService(BookingRepo bRepo, SeatService sServ) {
		super();
		this.bRepo = bRepo;
		this.sServ = sServ;
	}
	
	public void insertBooking(Booking booking) {
		System.out.println("All Booked Seats: " + booking.getSeats());
		for(Seat seat :booking.getSeats()) {
			seat.setBooked(true);
			this.sServ.insertSeat(seat);
		}
		
		bRepo.save(booking);
	}
	
	public List<Booking> findAll(){
		return bRepo.findAll();
	}
	
	public Booking findBookingByBookingId(int bookingId) {
		return bRepo.findBookingBybookingId(bookingId);
	}
	
}
