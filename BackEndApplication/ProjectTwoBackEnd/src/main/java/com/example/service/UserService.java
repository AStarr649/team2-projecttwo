package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.User;
import com.example.repository.UserRepo;

@Service
public class UserService {

	private UserRepo uRepo;
	
	public UserService() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public UserService(UserRepo uRepo) {
		super();
		this.uRepo = uRepo;
	}
	
	public List<User> getAllUsers(){
		return this.uRepo.findAll();
	}
	
	public void insertNewUser(User user) {
		this.uRepo.save(user);
	}
	
	public User getUserByUsername(String username) {
		return uRepo.findByUsername(username);
	}
	
	public User verifyUserLogin(String username, String password) {
		User user = uRepo.findByUsername(username);
		System.out.println("Inside VerifyUserLogin in userService: " + user  );
		if(user != null && user.getPassword().equals(password)) {
			return user;
		}
		return null;
	}
	
	public User findUserByEmail(String email) {
		return uRepo.findByEmail(email);
	}
	
	public void changeUserPassword(String password, int id) {
		User user = uRepo.getById(id);
		user.setPassword(password);
		uRepo.save(user);
	}
}
