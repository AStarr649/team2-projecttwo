package com.example.service;

import java.io.UnsupportedEncodingException;
import java.util.Random;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.example.model.Mail;
import com.example.repository.MailService;

@Service("mailService")
public class MailServImpl implements MailService{

	@Autowired
	JavaMailSender mailSender;
	
	public void sendMail(Mail mail, String randWord) {
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		
        try {
 
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
 
            mimeMessageHelper.setSubject("Password Reset Recently.");
            mimeMessageHelper.setFrom(new InternetAddress(mail.getMailFrom(), "APEGMovies.com"));
            mimeMessageHelper.setTo(mail.getMailTo());
            mimeMessageHelper.setText("Upon your request your password has been reset to:  "+ randWord +"."
            		+ "\n\nPlease reset password upon re-login.");
 
            mimeMessageHelper.toString();
            mailSender.send(mimeMessageHelper.getMimeMessage());
 
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
	}
	
	public String generateRandomWord() {
		String randomString;
		Random random = new Random();
		char[] word = new char[8];
		for (int j = 0; j < word.length; j++) {
			word[j] = (char) ('a' + random.nextInt(26));
		}
		randomString = new String(word);
		return randomString;
	}
}
