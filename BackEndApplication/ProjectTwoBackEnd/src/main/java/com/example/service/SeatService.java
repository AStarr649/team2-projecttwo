package com.example.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Seat;
import com.example.model.Show;
import com.example.repository.SeatRepo;

@Service
public class SeatService {
	private SeatRepo sRepo;
	
	public SeatService() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public SeatService(SeatRepo sRepo) {
		super();
		this.sRepo = sRepo;
	}
	
	public void insertSeat(Seat seat) {
		sRepo.save(seat);
	}
	
	public List<Seat> findAll(){
		return sRepo.findAll();
	}
	
	public Seat findBySeatId( int seatId) {
		return sRepo.findBySeatId(seatId);
	}
	
	public Seat findBySeatNumber(int seatNumber) {
		return sRepo.findBySeatNumber(seatNumber);
	}
	
	public List<Seat> findByIsBooked(boolean isBooked){
		return sRepo.findByIsBooked(isBooked);
	}
	
	public List<Seat> findByShow(Show show){
		List<Seat> sList = sRepo.findAll();
		List<Seat> sListTemp = new ArrayList<>();
		int totalNoOfSeat = show.gethHolder().getTotalSeat();
		for (int i = 0; i<totalNoOfSeat; i++) {
			sListTemp.add(null);
		}
		System.out.println("Size of sListTemp: " + sListTemp.size());
		for(Seat seat: sList) {
			if(seat.getShow().getShowId() == show.getShowId()) {
				sListTemp.set(seat.getSeatNumber()-1, seat);
			}
		}
		for(int i = 0; i<show.gethHolder().getTotalSeat(); i++) {
			if(sListTemp.get(i)== null) {
				Seat s = new Seat(i + 1, 25, false, show);
				sListTemp.set(i, s);
			}
		}
		return sListTemp;
	}

}
