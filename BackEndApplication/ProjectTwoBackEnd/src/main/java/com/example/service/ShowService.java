package com.example.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Show;
import com.example.repository.ShowRepo;

@Service
public class ShowService {

	private ShowRepo sRepo;

	public ShowService() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public ShowService(ShowRepo sRepo) {
		super();
		this.sRepo = sRepo;
	}

	public void insertShow(Show show) {
		sRepo.save(show);
	}

	public List<Show> findAllShow() {
		return sRepo.findAll();
	}

	public List<Show> findShowByShowdate(LocalDate showDate) {
		return sRepo.findByShowDate(showDate);
	}

	public List<Show> findShowByStarttime(LocalTime startTime) {
		return sRepo.findByStartTime(startTime);
	}

	public List<Show> findShowByMovieId(int movieId) {
		List<Show> showList = sRepo.findAll();
		List<Show> showListTemp = new ArrayList<>();
		for (Show show : showList) {
			if (show.getmHolder().getMovieId() == movieId) {
				showListTemp.add(show);
			}
		}
		return showListTemp;
	}

	public List<Show> findShowByHallId(int hallId) {
		List<Show> showList = sRepo.findAll();
		List<Show> showListTemp = new ArrayList<>();
		for (Show show : showList) {
			if (show.gethHolder().getHallId() == hallId) {
				showListTemp.add(show);
			}
		}
		return showListTemp;
	}

	public Show findShowByShowId(int showId) {

		return sRepo.findByShowId(showId);
	}

	public List<Show> findFeaturedShow() {
		List<Show> sList = sRepo.findAll();
		List<Show> sListTemp = new ArrayList<>();
		for (Show show : sList) {
			if (show.getShowDate().isEqual(LocalDate.now()) || show.getShowDate().isAfter(LocalDate.now())) {
				sListTemp.add(show);
			}
		}
		return sListTemp;
	}

}
