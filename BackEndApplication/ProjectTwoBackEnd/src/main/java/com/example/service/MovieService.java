package com.example.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Movie;
import com.example.model.Show;
import com.example.repository.MovieRepo;

@Service
public class MovieService {

	private MovieRepo mRepo;
	private ShowService sServ;
	
	public MovieService() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public MovieService(MovieRepo mRepo, ShowService sServ) {
		super();
		this.mRepo = mRepo;
		this.sServ = sServ;
	}
	
	public void insertMovie(Movie movie) {
		mRepo.save(movie);
	}
	
	public List<Movie> findAll(){
		return mRepo.findAll();
	}
	
	public Movie findByTitle(String title) {
		return mRepo.findByTitle(title);
	}
	
	public List<Movie> findByLanguage(String language){
		return mRepo.findByLanguage(language);
	}
	
	public List<Movie> findByCountry(String country){
		return mRepo.findByCountry(country);
	}
	
	public List<Movie> findByGenre(String genre){
		return mRepo.findByGenre(genre);
	}
	
	public List<Movie> getFeaturedMovies(){
		List<Movie> mList = new ArrayList<>();
		List<Show> sList = sServ.findFeaturedShow();
		for(Show show: sList) {
			if(!mList.contains(show.getmHolder())) {
				mList.add(show.getmHolder());
			}
		}
		return mList;
	}
	
	public List<Movie> getUpcommingMovies(){
		List<Movie> fMovieList = this.getFeaturedMovies();
		List<Movie> allMovieList = this.findAll();
		for(Movie m: fMovieList) {
			allMovieList.remove(m);
		}
		return allMovieList;
	}

	
	
	
	
	
	
}
