package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.Hall;
import com.example.repository.HallRepo;

@Service
public class HallService {
	
	private HallRepo hRepo;
	
	public HallService() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public HallService(HallRepo hRepo) {
		super();
		this.hRepo = hRepo;
	}
	
	public void insertHall(Hall hall) {
		hRepo.save(hall);
	}
	
	public List<Hall> findAll(){
		return this.hRepo.findAll();
	}
	
	public Hall findByName(String name) {
		return this.hRepo.findByName(name);
	}
	
	
	
}
