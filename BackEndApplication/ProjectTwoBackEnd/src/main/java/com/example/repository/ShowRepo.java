package com.example.repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.Show;

public interface ShowRepo extends JpaRepository<Show, Integer>{

	public Show findByShowId(int showId);
	public List<Show> findByShowDate(LocalDate showDate);
	public List<Show> findByStartTime(LocalTime startTime);
	
}
