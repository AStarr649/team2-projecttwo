package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.Movie;

public interface MovieRepo extends JpaRepository<Movie, Integer>{

	
	
	public Movie findByTitle(String title);
	public List<Movie> findByLanguage(String language);
	public List<Movie> findByCountry(String country);
	public List<Movie> findByGenre(String genre);
	
}
