package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.Seat;

public interface SeatRepo extends JpaRepository<Seat, Integer> {
	public Seat findBySeatId(int seatId);
	public Seat findBySeatNumber(int seatNumber);
	public List<Seat> findByIsBooked(boolean isBooked);
	
}
