package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.Booking;

public interface BookingRepo extends JpaRepository<Booking, Integer> {

	public Booking findBookingBybookingId(int bookingId);
}
