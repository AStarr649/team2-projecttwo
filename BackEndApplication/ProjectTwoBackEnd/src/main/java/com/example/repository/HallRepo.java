package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.Hall;

public interface HallRepo extends JpaRepository<Hall, Integer>{

	public Hall findByName(String name);
	
}
