package com.example.repository;

import com.example.model.Mail;

public interface MailService {

	public void sendMail(Mail mail, String randWord);
}
