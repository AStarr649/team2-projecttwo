package com.example.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="seat")
public class Seat {
	@Id
	@Column(name="seat_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int seatId;
	
	@Column(name="seat_number")
	private int seatNumber;
	
	@Column(name="price")
	private double price;
	
	@Column(name="isbooked")
	private boolean isBooked;
	
	@ManyToOne(cascade=CascadeType.MERGE, fetch=FetchType.EAGER)
	@JoinColumn(name="show_fk")
	private Show sHolder;
	
	public Seat() {
		// TODO Auto-generated constructor stub
	}

	public Seat(int seatNumber, double price, boolean isBooked, Show show) {
		super();
		this.seatNumber = seatNumber;
		this.price = price;
		this.isBooked = isBooked;
		this.sHolder = show;
	}
	
	public Seat(int seatId, int seatNumber, double price, boolean isBooked, Show show) {
		super();
		this.seatId = seatId;
		this.seatNumber = seatNumber;
		this.price = price;
		this.isBooked = isBooked;
		this.sHolder = show;
	}

	public int getSeatNumber() {
		return seatNumber;
	}

	public void setSeatNumber(int seatNumber) {
		this.seatNumber = seatNumber;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public boolean isBooked() {
		return isBooked;
	}

	public void setBooked(boolean isBooked) {
		this.isBooked = isBooked;
	}

	public Show getShow() {
		return sHolder;
	}

	public void setShow(Show show) {
		this.sHolder = show;
	}

	public int getSeatId() {
		return seatId;
	}

	@Override
	public String toString() {
		return "Seat [seatId=" + seatId + ", seatNumber=" + seatNumber + ", price=" + price + ", isBooked=" + isBooked
				+ ", show=" + sHolder + "]"+"\n";
	}
	
	
	
	
}
