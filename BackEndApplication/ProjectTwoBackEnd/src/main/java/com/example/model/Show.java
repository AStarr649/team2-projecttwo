package com.example.model;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;

@Entity
@Table(name="show")
public class Show {

	@Id
	@Column(name="show_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int showId;
	
	@Column(name="show_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate showDate;
	
	@Column(name="start_time")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
	@JsonDeserialize(using = LocalTimeDeserializer.class)
	@JsonSerialize(using = LocalTimeSerializer.class)
	private LocalTime startTime;
	
	@OneToOne(cascade=CascadeType.MERGE, fetch=FetchType.EAGER)
	@JoinColumn(name="hall_fk")
	private Hall hHolder;
	
	@ManyToOne(cascade=CascadeType.MERGE, fetch=FetchType.EAGER)
	@JoinColumn(name="movie_fk")
	private Movie mHolder;
	
	public Show() {
		// TODO Auto-generated constructor stub
	}

	public Show(LocalDate showDate, LocalTime startTime, Hall hHolder, Movie mHolder) {
		super();
		this.showDate = showDate;
		this.startTime = startTime;
		this.hHolder = hHolder;
		this.mHolder = mHolder;
	}
	
	
	public Show(int showId, LocalDate showDate, LocalTime startTime, Hall hHolder, Movie mHolder) {
		super();
		this.showId = showId;
		this.showDate = showDate;
		this.startTime = startTime;
		this.hHolder = hHolder;
		this.mHolder = mHolder;
	}

	public LocalDate getShowDate() {
		return showDate;
	}

	public void setDate(LocalDate showDate) {
		this.showDate = showDate;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	

	public int getShowId() {
		return showId;
	}

	@Override
	public String toString() {
		return "Show [showId=" + showId + ", date=" + showDate + ", startTime=" + startTime + ", hallId=" + hHolder
				+ ", movieId=" + mHolder + "]";
	}

	public Hall gethHolder() {
		return hHolder;
	}

	public void sethHolder(Hall hHolder) {
		this.hHolder = hHolder;
	}

	public Movie getmHolder() {
		return mHolder;
	}

	public void setmHolder(Movie mHolder) {
		this.mHolder = mHolder;
	}
	
	
	
	
	
}
