package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="hall")
public class Hall {
	
	@Id
	@Column(name="hall_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int hallId;
	
	@Column(name="name", unique=true, nullable=false)
	private String name;
	
	@Column(name="total_seat")
	private int totalSeat;
	
	
	public Hall() {
		// TODO Auto-generated constructor stub
	}
	
	public Hall(String name, int totalSeat) {
		super();
		this.name = name;
		this.totalSeat = totalSeat;
	}

	public Hall(int hallId, String name, int totalSeat) {
		super();
		this.hallId = hallId;
		this.name = name;
		this.totalSeat = totalSeat;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTotalSeat() {
		return totalSeat;
	}

	public void setTotalSeat(int totalSeat) {
		this.totalSeat = totalSeat;
	}

	public int getHallId() {
		return hallId;
	}

	@Override
	public String toString() {
		return "Hall [hallId=" + hallId + ", name=" + name + ", totalSeat=" + totalSeat + "]";
	}
	
	
	
	
	
}
