package com.example.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

@Entity
@Table(name="booking")
public class Booking {

	@Id
	@Column(name="booking_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int bookingId;
	
	@Column(name="booking_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate bookingDate;
	
	@OneToOne(cascade=CascadeType.MERGE, fetch=FetchType.EAGER)
	@JoinColumn(name="show_fk")
	private Show sHolder;
	
	@ManyToOne(cascade=CascadeType.MERGE, fetch=FetchType.EAGER)
	@JoinColumn(name="user_fk")
	private User uHolder;
	
	@OneToMany(cascade=CascadeType.MERGE, fetch=FetchType.EAGER)
	private List<Seat> seats;
	
	public Booking() {
		// TODO Auto-generated constructor stub
	}
	
	public Booking(Show sHolder, User uHolder, List<Seat> seats) {
		super();
		this.bookingDate = LocalDate.now();
		this.sHolder = sHolder;
		this.uHolder = uHolder;
		this.seats = seats;
	}

	public Booking(LocalDate bookingDate, Show sHolder, User uHolder, List<Seat> seats) {
		super();
		this.bookingDate = bookingDate;
		this.sHolder = sHolder;
		this.uHolder = uHolder;
		this.seats = seats;
	}
	
	public Booking(int bookingId, LocalDate bookingDate, Show sHolder, User uHolder, List<Seat> seats) {
		super();
		this.bookingId = bookingId;
		this.bookingDate = bookingDate;
		this.sHolder = sHolder;
		this.uHolder = uHolder;
		this.seats = seats;
	}

	public LocalDate getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(LocalDate bookingDate) {
		this.bookingDate = bookingDate;
	}

	public Show getsHolder() {
		return sHolder;
	}

	public void setsHolder(Show sHolder) {
		this.sHolder = sHolder;
	}

	public User getuHolder() {
		return uHolder;
	}

	public void setuHolder(User uHolder) {
		this.uHolder = uHolder;
	}

	public List<Seat> getSeats() {
		return seats;
	}

	public void setSeats(List<Seat> seats) {
		this.seats = seats;
	}

	public int getBookingId() {
		return bookingId;
	}

	@Override
	public String toString() {
		return "Booking [bookingId=" + bookingId + ", bookingDate=" + bookingDate + ", sHolder=" + sHolder
				+ ", uHolder=" + uHolder + ", seats=" + seats + "]";
	}	
	
}
