package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Show;
import com.example.service.ShowService;

@RestController
@RequestMapping(value="/shows")
@CrossOrigin(origins="*")
public class ShowController {
	
	private ShowService sServ;
	
	public ShowController() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public ShowController(ShowService sServ) {
		super();
		this.sServ = sServ;
	}
	
	@GetMapping() //GET at localhost:9065/shows
	public ResponseEntity<List<Show>> allShows(){
		return ResponseEntity.status(200).body(this.sServ.findAllShow());
	}
	
	@GetMapping(value="/show/{showId}") //GET at localhost:9065/shows/show/{showId}
	public ResponseEntity<Show> findShowByShowId(@PathVariable("showId") int showId){
		return ResponseEntity.status(200).body(this.sServ.findShowByShowId(showId));
	}
	
	@GetMapping(value="/movie/{movieId}") //GET at localhost:9065/shows/movie/{movieId}
	public ResponseEntity<List<Show>> findShowByMovieId(@PathVariable("movieId") int movieId){
		return ResponseEntity.status(200).body(this.sServ.findShowByMovieId(movieId));
	}
	
	@GetMapping(value="/hall/{hallId}")// GET at localhost:9065/shows/hall/{hallId}
	public ResponseEntity<List<Show>> findShowByHallId(@PathVariable("hallId") int hallId){
		return ResponseEntity.status(200).body(this.sServ.findShowByHallId(hallId));
	}
	
	@PostMapping() //POST localhost:9065/shows
	public ResponseEntity<Show> addNewShow(@RequestBody Show show){
		this.sServ.insertShow(show);
		return ResponseEntity.status(201).body(sServ.findShowByShowId(show.getShowId()));
	}


	
	

}
