package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Movie;
import com.example.service.MovieService;

@RestController
@RequestMapping(value="/movies")
@CrossOrigin(origins="*")
public class MovieController {

	private MovieService mServ;
	
	public MovieController() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public MovieController(MovieService mServ) {
		super();
		this.mServ = mServ;
	}
	
	@GetMapping() //GET at localhost:9025/movies
	public ResponseEntity<List<Movie>> allMovies(){
		return ResponseEntity.status(200).body(this.mServ.findAll());
	}
	
	@GetMapping(value="/featured") //GET localhost:9065/movies/featured
	public ResponseEntity<List<Movie>> getFeaturedMovies(){
		return ResponseEntity.status(200).body(this.mServ.getFeaturedMovies());
	}
	
	@GetMapping(value="/upcoming") //GET localhost:9065/movies/upcoming
	public ResponseEntity<List<Movie>> getUpcomingMovies(){
		return ResponseEntity.status(200).body(this.mServ.getUpcommingMovies());
	}

	
	
}
