package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Seat;
import com.example.model.Show;
import com.example.service.SeatService;

@RestController
@RequestMapping(value="/seats")
@CrossOrigin(origins="*")
public class SeatController {
	private SeatService sServ;
	
	public SeatController() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public SeatController(SeatService sServ) {
		super();
		this.sServ = sServ;
	}
	
	@GetMapping()
	public ResponseEntity<List<Seat>> allSeat(){
		return ResponseEntity.status(200).body(this.sServ.findAll());
	}
	
	@PostMapping(value="/show")
	public ResponseEntity<List<Seat>> findByShow(@RequestBody Show show){
		System.out.println(show);
		return ResponseEntity.status(200).body(this.sServ.findByShow(show));
	}

}
