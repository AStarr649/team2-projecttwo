package com.example.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Booking;
import com.example.service.BookingService;

@RestController
@RequestMapping(value="/bookings")
@CrossOrigin(origins="*")
public class BookingController {

	private BookingService bServ;
	
	public BookingController() {
		// TODO Auto-generated constructor stub
	}

	@Autowired 
	public BookingController(BookingService bServ) {
		super();
		this.bServ = bServ;
	}
	
	@PostMapping()//POST localhost:9065/bookings
	public ResponseEntity<Booking> addNewBooking(@RequestBody Booking booking){
		booking.setBookingDate(LocalDate.now());
		this.bServ.insertBooking(booking);
		return ResponseEntity.status(201).body(this.bServ.findBookingByBookingId(booking.getBookingId()));
	}

	@GetMapping()
	public ResponseEntity<List<Booking>> allBooking(){
		return ResponseEntity.status(200).body(this.bServ.findAll());
	}
	
	
	
}
