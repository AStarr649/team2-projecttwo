package com.example.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Mail;
import com.example.model.User;
import com.example.service.MailServImpl;
import com.example.service.UserService;

@RestController
@RequestMapping(value="/users")
@CrossOrigin(origins="*")
public class UserController {

	private UserService uServ;
	private MailServImpl mServ;
	
	public UserController() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public UserController(UserService uServ, MailServImpl mServ) {
		super();
		this.uServ = uServ;
		this.mServ = mServ;
	}
	
	@GetMapping()
	public ResponseEntity<List<User>> allUsers(){
		return ResponseEntity.status(200).body(this.uServ.getAllUsers());
	}
	
	@GetMapping(value="/user/{username}")
	public ResponseEntity<User> getUserByUsername(@PathVariable("username") String username){
		Optional<User> userOpt = Optional.ofNullable(this.uServ.getUserByUsername(username));
		if(userOpt.isPresent()) {
			return ResponseEntity.status(200).body(userOpt.get());
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PostMapping()
	public ResponseEntity<User> addNewUser(@RequestBody User user){
		Optional<User> userOpt = Optional.ofNullable(this.uServ.getUserByUsername(user.getUsername()));
		if(userOpt.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		uServ.insertNewUser(user);
		return ResponseEntity.status(201).body(uServ.getUserByUsername(user.getUsername()));
	}
	
	@PostMapping(value="/login", consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> verifyUser(@RequestBody LinkedHashMap bodyMap){
		Optional<User> userTemp = Optional.ofNullable(this.uServ.verifyUserLogin(bodyMap.get("username").toString(), bodyMap.get("password").toString()));
		if(userTemp.isPresent()) {
			return ResponseEntity.status(200).body(userTemp.get());
		}
		return ResponseEntity.notFound().build();		
	}
	
	@PostMapping(value="/user/pwreset/{username}")
	public ResponseEntity<String> resetPassword(@PathVariable("username") String username) {
		System.out.println(username);
		User user = this.uServ.getUserByUsername(username);
		System.out.println(user);
		if(user == null) {
			return ResponseEntity.notFound().build();
		}
		Mail mail = new Mail();
		mail.setMailFrom("javasqlnauts@gmail.com");
		mail.setMailTo(user.getEmail());
		String randWord = mServ.generateRandomWord();
		this.mServ.sendMail(mail, randWord);
		changeUserPassword(user, randWord);
		return ResponseEntity.status(200).body("Success");
	}
	
	@PostMapping(value="/user/newpassword")
	public ResponseEntity<User> changePassword(@RequestBody LinkedHashMap bodyMap){
		User user = this.uServ.getUserByUsername(bodyMap.get("userName").toString());
		if(user.getPassword().equals(bodyMap.get("oldPassword").toString())) {
			changeUserPassword(user, bodyMap.get("newPassword").toString());
			return ResponseEntity.status(201).body(user);
		}else {
			return ResponseEntity.badRequest().build();
		}
	}

	public void changeUserPassword(User user, String password) {	
		this.uServ.changeUserPassword(password, user.getUserId());
	}
}
