package com.example.controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Booking;
import com.example.model.Hall;
import com.example.model.Movie;
import com.example.model.Seat;
import com.example.model.Show;
import com.example.model.User;
import com.example.service.BookingService;
import com.example.service.HallService;
import com.example.service.MovieService;
import com.example.service.SeatService;
import com.example.service.ShowService;
import com.example.service.UserService;

@RestController
@RequestMapping(value = "/orm")
public class ORMController {

	private MovieService mServ;
	private UserService uServ;
	private HallService hServ;
	private ShowService sServ;
	private SeatService seatServ;
	private BookingService bServ;

	public ORMController() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public ORMController(MovieService mServ, UserService uServ, HallService hServ, ShowService sServ,
			SeatService seatServ, BookingService bServ) {
		super();
		this.mServ = mServ;
		this.uServ = uServ;
		this.hServ = hServ;
		this.sServ = sServ;
		this.seatServ = seatServ;
		this.bServ = bServ;
	}

	@GetMapping(value = "/init")
	public ResponseEntity<String> insertInitialValues() {

		// Movie Creation
		List<Movie> mList = new ArrayList<>(Arrays.asList(
				new Movie("Spider-Man: No Way Home", "With Spider-Man's identity now revealed, our friendly neighborhood web-slinger is unmasked and no longer able to separate his normal life as Peter Parker from the high stakes of being a superhero. When Peter asks for help from Doctor Strange, the stakes become even more dangerous, forcing him to discover what it truly means to be Spider-Man.", LocalTime.of(2, 30), "PG13", LocalDate.of(2021,12,17),
						"American", "Science Friction"),
				new Movie("Sing 2", "Can-do koala Buster Moon and his all-star cast of animal performers prepare to launch a dazzling stage extravaganza in the glittering entertainment capital of the world. There's just one hitch -- he has to find and persuade the world's most reclusive rock star to join them. What begins as Buster's dream of big-time success soon becomes an emotional reminder of the power of music to heal even the most broken heart.", LocalTime.of(1, 50), "PG", LocalDate.of(2021,12,22), "American",
						"Science Friction"),	
				new Movie("Moonfall", "The world stands on the brink of annihilation when a mysterious force knocks the moon from its orbit and sends it hurtling toward a collision course with Earth. With only weeks before impact, NASA executive Jocinda \"Jo\" Fowler teams up with a man from her past and a conspiracy theorist for an impossible mission into space to save humanity.", LocalTime.of(2, 10), "PG13", LocalDate.of(2022,02,04), "American",
						"Comic Movie"),
				new Movie("Uncharted", "Treasure hunter Victor \"Sully\" Sullivan recruits street-smart Nathan Drake to help him recover a 500-year-old lost fortune amassed by explorer Ferdinand Magellan. What starts out as a heist soon becomes a globe-trotting, white-knuckle race to reach the prize before the ruthless Santiago Moncada can get his hands on it.", LocalTime.of(1, 55), "PG13", LocalDate.of(2022, 02, 18), "American",
						"Comic Movie"),
				new Movie("The Batman", "Batman is a superhero who appears in American comic books published by DC Comics.", LocalTime.of(2, 15), "English", LocalDate.of(2022, 04, 02), "American",
						"Comic Movie"),
				new Movie("Jurrasic World: Dominion", "Third instalment of the Jurassic World franchise. Worldwide release date: june 2022.", LocalTime.of(2, 30), "English", LocalDate.of(2022, 05, 25), "American",
						"Comic Movie"),
				new Movie("The Lost City", "The Lost City is set to have its world premiere at South by Southwest on March 12, 2022", LocalTime.of(2, 50), "English", LocalDate.of(2022, 01, 25) , "American",
						"Comic Movie")));
		// Populating movie Table
		for (Movie movie : mList) {
			this.mServ.insertMovie(movie);
		}

		// User Creation
		User user1 = new User("guest046", "guest046", "firstname", "lastname", "g@gmail.com");
		User user2 = new User("william", "smith", "William", "Smith", "wsmith@gmail.com");
		User user3 = new User("mike", "green", "Mike", "Green", "mgreen@gmail.com");
		
		// Populating user_table Table
		this.uServ.insertNewUser(user1);
		this.uServ.insertNewUser(user2);

		// Hall Creation
		Hall hall1 = new Hall("Thunder", 40);
		Hall hall2 = new Hall("Black Diamond", 40);

		// Show Creation
		Show show1 = new Show(LocalDate.now(), LocalTime.of(13, 0), hall1, mList.get(0));
		Show show2 = new Show(LocalDate.now(), LocalTime.of(15, 0), hall2, mList.get(0));
		Show show3 = new Show(LocalDate.now(), LocalTime.of(19, 0), hall2, mList.get(0));

		Show show4 = new Show(LocalDate.now(), LocalTime.of(13, 0), hall1, mList.get(1));
		Show show5 = new Show(LocalDate.now(), LocalTime.of(15, 0), hall2, mList.get(1));
		Show show6 = new Show(LocalDate.now(), LocalTime.of(19, 0), hall2, mList.get(1));

		Show show7 = new Show(LocalDate.now(), LocalTime.of(13, 0), hall1, mList.get(2));
		Show show8 = new Show(LocalDate.now(), LocalTime.of(15, 0), hall2, mList.get(2));
		Show show9 = new Show(LocalDate.now(), LocalTime.of(19, 0), hall2, mList.get(2));
		
		Show show10 = new Show(LocalDate.now(), LocalTime.of(13, 0), hall1, mList.get(3));
		Show show11 = new Show(LocalDate.now(), LocalTime.of(15, 0), hall2, mList.get(3));
		Show show12 = new Show(LocalDate.now(), LocalTime.of(19, 0), hall2, mList.get(3));

		// Seat creation which is not booked
//		for(int i=1;i<=10;i++) {
//			Seat seat = new Seat(i, 25.00, false, show1);
//			seatServ.insertSeat(seat);
//		}
		// Seat creation which is booked

		List<Seat> sList1 = new ArrayList<>();
		List<Seat> sList2 = new ArrayList<>();
		for (int i = 11; i <= 15; i++) {
			Seat seat = new Seat(i, 25.00, true, show1);
			if (i == 11 || i == 12) {
				sList1.add(seat);
			} else {
				sList2.add(seat);
			}
			// seatServ.insertSeat(seat);
		}

		// Populating hall Table
		hServ.insertHall(hall1);
		hServ.insertHall(hall2);

		// Populating show Table
		sServ.insertShow(show1);
		sServ.insertShow(show2);
		sServ.insertShow(show3);

		sServ.insertShow(show4);
		sServ.insertShow(show5);
		sServ.insertShow(show6);

		sServ.insertShow(show7);
		sServ.insertShow(show8);
		sServ.insertShow(show9);
		
		sServ.insertShow(show10);
		sServ.insertShow(show11);
		sServ.insertShow(show12);

		// Booking Creation
		Booking booking1 = new Booking(LocalDate.now(), show1, user1, sList1);
		Booking booking2 = new Booking(LocalDate.now(), show1, user2, sList2);
		bServ.insertBooking(booking1);
		bServ.insertBooking(booking2);

		return ResponseEntity.status(201).body("Successfully Inserted");
	}

}
