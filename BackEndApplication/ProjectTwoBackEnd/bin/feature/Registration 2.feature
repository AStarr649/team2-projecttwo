#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

	Feature: A user is able to register into the movie theatre application

  
  Scenario: Successful registration of the movie application 
    Given The user is at the homepage of the movie website
    When the user registers from the homepage  
    And a user inputs their first name
    And a user inputs their last name 
    And a user inputs their e-mail
    And a user inputs their password
    Then a user has successfully registered 
   	But a user submits form 
		Then the user is redirected to the login 
  
  
  
  
  Scenario Outline: Unsuccessful registration ino the movie theatre application 
     	Given The user is at the homepage of the movie website
   		When a user incorrectly inputs their user name
   		And a user incorrectly inputs their password
   		Then the user should see the span of Enter your userName and password correct
 

    Examples: 
 			| username  | password  |
 			| 	        |           |
 			| invalidUN | invalidPW |
