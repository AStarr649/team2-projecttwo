Feature: A User can select a movie and show time to purchase ticket(s)

Background: A user is logged in on the home page
		Given The user is on the login page
		When the user inputs "JaneDoe" into the username field
		When the user inputs "password" into the password field
		When the user clicks login
		Then the user is directed to the home page

  Scenario: A user can view all current movies playing
    When movie home page loads
    Then user should see current showing movies
    
    Scenario: A user can click a movie to see show times
    	When user clicks movie
    	Then user should see show times available
    	
    	Scenario: A user can click a show time to purchase tickets
    		When user clicks time from selected movie
    		Then user should be directed to the ticket purchase page

