Feature: A user can select a seat and purchase a ticket for that seat

Background: A user is logged in on the home page
		Given The user is on the login page
		When the user inputs "JaneDoe" into the username field
		When the user inputs "password" into the password field
		When the user clicks login
		Then the user is directed to the home page
		When the user clicks a movie
		And the user selects a time
		Then the user is directed to seat selection page
		
  	Scenario: A user is able to choose a seat(s) to get a ticket for
    	When the user clicks on a desired seat
    	Then the user is able to see ticket price for selected seat(s)
    	
    	Scenario: A user can choose to purchase selected ticket(s)
    		When the user clicks on purchase tickets
    		Then the user is informed of purchase
    		And the user is redirected back to the home page

  